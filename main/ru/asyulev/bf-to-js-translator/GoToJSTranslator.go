package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
)

type Instruction struct {
	operator uint16
	operand  uint16
}

const headerJsCode = "const MEMORY_SIZE = 30000;\nconst memory = new Uint8Array(MEMORY_SIZE).fill(0);\n" +
	"\n// Memory pointer (Points to a cell in MEMORY)\nlet mpointer = 0;" +
	"\n\n// Output text" +
	"\nlet output = \"\";" +
	"\n\n// Instruction pointer (Points to the current INSTRUCTION)" +
	"\nlet ipointer = 0;" +
	"\n\nfunction sendOutput() {\n    output += String.fromCharCode(memory[mpointer]);\n}\n\n" +
	"\n\nfunction mpPlus() {\n	mpointer++;\n}" +
	"\n\nfunction mpMinus() {\n	mpointer--;\n}" +
	"\n\nfunction mvPlus() {\n	memory[mpointer]++;\n}" +
	"\n\nfunction mvMinus() {\n	memory[mpointer]--;\n}" +
	"\n\nfunction mvInput(value) {\n	memory[mpointer] = value;\n}" +
	"\n\nfunction jmpFwdFnc(value) {\n	if (!memory[mpointer]) {\n		ipointer = value;\n	}\n}" +
	"\n\nfunction jmpBckFnc(value) {\n	if (memory[mpointer]) {\n		ipointer = value;\n	}\n}" +
	"\n\nlet funArr = [\n"
const srcContentPath = "/main/resource/bf-source/source.b"
const inputContentPath = "/main/resource/bf-source/input.txt"
const saveJSPath = "/main/resource/js-output/output.js"
const (
	opIncDp = iota
	opDecDp
	opIncVal
	opDecVal
	opOut
	opIn
	opJmpFwd
	opJmpBck
)

func main() {

	var sourceContent = readFileToString(srcContentPath)
	var inputContent = readFileToString(inputContentPath)
	var res, parseErr = parseBf(sourceContent, inputContent)

	print(res)
	saveJSScript(res, saveJSPath)
	if parseErr != nil {
		fmt.Println(parseErr)
		return
	}
}

func parseBf(bfCode string, inputVals string) (jsCode string, err error) {
	var pc, jmpPc uint16 = 0, 0

	var instrArr []Instruction
	jmpStack := make([]uint16, 0)
	for _, c := range bfCode {
		switch c {
		case '>':
			instrArr = append(instrArr, Instruction{opIncDp, 0})
		case '<':
			instrArr = append(instrArr, Instruction{opDecDp, 0})
		case '+':
			instrArr = append(instrArr, Instruction{opIncVal, 0})
		case '-':
			instrArr = append(instrArr, Instruction{opDecVal, 0})
		case '.':
			instrArr = append(instrArr, Instruction{opOut, 0})
		case ',':
			instrArr = append(instrArr, Instruction{opIn, 0})
		case '[':
			instrArr = append(instrArr, Instruction{opJmpFwd, 0})
			jmpStack = append(jmpStack, pc)
		case ']':
			if len(jmpStack) == 0 {
				return "nil", errors.New("close square bracket when there is no open bracket")
			}
			jmpPc = jmpStack[len(jmpStack)-1]
			jmpStack = jmpStack[:len(jmpStack)-1]
			instrArr = append(instrArr, Instruction{opJmpBck, jmpPc})
			instrArr[jmpPc].operand = pc
		default:
			pc--
		}
		pc++
	}
	if len(jmpStack) != 0 {
		return "nil", errors.New("open square brackets left")
	}

	return createJSCode(instrArr, inputVals)
}

func createJSCode(instr []Instruction, inputVals string) (jsCode string, err error) {
	var idx = 0
	jsCode = headerJsCode
	for pc := 0; pc < len(instr); pc++ {
		switch instr[pc].operator {
		case opIncDp:
			jsCode += "	mpPlus,\n"
		case opDecDp:
			jsCode += "	mpMinus,\n"
		case opIncVal:
			jsCode += "	mvPlus,\n"
		case opDecVal:
			jsCode += "	mvMinus,\n"
		case opOut:
			jsCode += "	sendOutput,\n"
		case opIn:
			jsCode += "	function() { mvInput('" + strconv.Itoa(int(inputVals[idx])) + "')},\n"
		case opJmpFwd:
			jsCode += "	function() { jmpFwdFnc(" + strconv.Itoa(int(instr[pc].operand)) + ") },\n"
		case opJmpBck:
			jsCode += "	function() { jmpBckFnc(" + strconv.Itoa(int(instr[pc].operand)) + ") },\n"
		default:
			panic("Unknown operator.")
		}
	}
	jsCode += "];\n\nfor (; ipointer < funArr.length; ipointer++) {\n    funArr[ipointer]();\n}\n\nconsole.log(output);\n"
	return jsCode, nil
}

func readFileToString(path string) (content string) {

	pwd, _ := os.Getwd()
	fullPath := filepath.Join(pwd, path)
	file, err := os.Open(fullPath)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	b, err := ioutil.ReadAll(file)
	return string(b)
}

func saveJSScript(source string, path string) {
	pwd, _ := os.Getwd()
	fullPath := filepath.Join(pwd, path)
	var err = ioutil.WriteFile(fullPath, []byte(source), 0644)
	if err != nil {
		panic(err)
	}
}
